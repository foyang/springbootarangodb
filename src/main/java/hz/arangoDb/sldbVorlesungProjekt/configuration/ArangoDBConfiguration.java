package hz.arangoDb.sldbVorlesungProjekt.configuration;

import com.arangodb.ArangoDB;
import com.arangodb.Protocol;
import com.arangodb.springframework.annotation.EnableArangoRepositories;
import com.arangodb.springframework.config.AbstractArangoConfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableArangoRepositories(basePackages = { "hz.arangoDb.sldbVorlesungProjekt" })
public class ArangoDBConfiguration extends AbstractArangoConfiguration {
    /**
     * Database url (server + port) & credentials configs
     * @return
     */
    @Override
    public ArangoDB.Builder arango() {
        ArangoDB.Builder arango = new ArangoDB.Builder()
                .host("10.20.110.48", 8529)
                .user("")
                .password("");
        return arango;
    }

    /**
     * Database name
     * @return
     */
    @Override
    public String database() {
       // return "arangoDBDemo";
        return "police_register";
    }
}
