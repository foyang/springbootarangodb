package hz.arangoDb.sldbVorlesungProjekt.service;

import hz.arangoDb.sldbVorlesungProjekt.collection.Car;
import hz.arangoDb.sldbVorlesungProjekt.collection.Event;
import hz.arangoDb.sldbVorlesungProjekt.collection.GraphModelEvent;
import hz.arangoDb.sldbVorlesungProjekt.collection.payload.GraphPayload;
import hz.arangoDb.sldbVorlesungProjekt.collection.repository.CarRepository;
import hz.arangoDb.sldbVorlesungProjekt.collection.repository.EventRepository;
import hz.arangoDb.sldbVorlesungProjekt.restController.CarController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Service
public class CarService {

    //TODO Add methode desecription

    private final Logger log = LoggerFactory.getLogger(CarController.class);

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private EventRepository eventRepository;


    /**
     * Find all saved articles so far
     * @return
     */
    public List<Car> findAll() {
        return (List<Car>) carRepository.findAll();
    }

    public Car save(Car car) {
        return carRepository.save(car);
    }

    public void deleteAllCars(List<Car> carList) {
        carRepository.deleteAll(carList);
    }

    public List<String> getEventModelList(){
        List<String> modelList = new ArrayList<>();
        List<Car> carList = (List<Car>) carRepository.findAll();
        for (Car car:carList){
            modelList.add(car.getModel());
        }
        return modelList;
    }

    public GraphPayload getGraphFromCarByModel(String model){
        List<Car> carsByModel = carRepository.findAllByModel(model);
        List<Event> events = (List<Event>) eventRepository.findAll();
        List<String> finalCarModelList = new ArrayList<>();
        List<String> finalCarModelNoRepeatList = new ArrayList<>();
        GraphPayload graphPayload = new GraphPayload();
        long time = System.currentTimeMillis();
        try {
            for (Car car:carsByModel){
                for (Event event:events){
                    if (event.get_from().equals("cars/"+car.getId())){
                        Optional<Car> currentCar = carRepository.findById(event.get_to());
                        if (currentCar.isPresent()){
                            Car currentCarTo = currentCar.get();
                            finalCarModelList.add(currentCarTo.getModel());
                            if (!finalCarModelNoRepeatList.contains(currentCarTo.getModel())){
                                finalCarModelNoRepeatList.add(currentCarTo.getModel());
                            }
                        }
                    }
                }
            }
        } finally {
            time = System.currentTimeMillis() - time;
            graphPayload.setTime(time);
        }
        List<GraphModelEvent> graph = getGraph(finalCarModelNoRepeatList, finalCarModelList);
        graphPayload.setGraphModelEvents(graph);
        return graphPayload;
    }

    private List<GraphModelEvent> getGraph(List<String> finalCarModelNoRepeatList, List<String> finalCarModelList){
        List<GraphModelEvent> graphCarsModelEventList = new ArrayList<>();
        for (String modelString:finalCarModelNoRepeatList){
            GraphModelEvent graphCarsModelEvent = new GraphModelEvent();
            int countModel = 0;
            for (String evModel:finalCarModelList){
                if (evModel.equals(modelString)){
                    countModel++;
                }
            }
            graphCarsModelEvent.setModel(modelString);
            graphCarsModelEvent.setCount(countModel);
            graphCarsModelEventList.add(graphCarsModelEvent);
        }
        return graphCarsModelEventList;
    }
}
