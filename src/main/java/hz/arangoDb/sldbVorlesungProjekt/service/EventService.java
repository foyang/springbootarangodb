package hz.arangoDb.sldbVorlesungProjekt.service;

import hz.arangoDb.sldbVorlesungProjekt.collection.Car;
import hz.arangoDb.sldbVorlesungProjekt.collection.Event;
import hz.arangoDb.sldbVorlesungProjekt.collection.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class EventService {

    //TODO Add methode desecription

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private CarService carService;

    public Iterable<Event> findAll() {
        return eventRepository.findAll();
    }

    public Event save(Event event) {
        return eventRepository.save(event);
    }
    public void delete(List<Event> event){
        eventRepository.deleteAll(event);
    }

    public List<Event> getEvents(){
        return (List<Event>) eventRepository.findAll();
    }

    public List<Car> getRandomCar(int randomNumber){
        Random rand = new Random();
        List<Car> carRandomList = new ArrayList<>();
        List<Car> carList = carService.findAll();
        for (int i = 0; i<randomNumber;i++){
            int randomIndex = rand.nextInt(carList.size());
            Car randomElement = carList.get(randomIndex);
            carRandomList.add(randomElement);
            carList.remove(randomIndex);
        }
        return carRandomList;
    }
}
