package hz.arangoDb.sldbVorlesungProjekt.collection.payload;

import hz.arangoDb.sldbVorlesungProjekt.collection.Event;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@ToString
@Getter
@Setter
public class EventsPayload {
    private long time;
    private List<Event> events;
}
