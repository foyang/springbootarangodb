package hz.arangoDb.sldbVorlesungProjekt.collection.payload;

import hz.arangoDb.sldbVorlesungProjekt.collection.Car;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import java.util.List;

@ToString
@Setter
@Getter
public class CarsPayload {
    private long time;
    private List<Car> cars;
}
