package hz.arangoDb.sldbVorlesungProjekt.collection.payload;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Getter
public class CarPayload {

    private String id;
    private long time;
    private String model;
    private String color;
    private int year;
}
