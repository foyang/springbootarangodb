package hz.arangoDb.sldbVorlesungProjekt.collection.payload;

import hz.arangoDb.sldbVorlesungProjekt.collection.GraphModelEvent;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@ToString
@Getter
@Setter
public class GraphPayload {
    private long time;
    List<GraphModelEvent> graphModelEvents;
}
