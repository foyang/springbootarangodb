package hz.arangoDb.sldbVorlesungProjekt.collection.payload;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Getter
public class EventPayload {
    private String id;
    private long time;
    private String type;
    private String _from;
    private String _to;
}
