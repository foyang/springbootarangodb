package hz.arangoDb.sldbVorlesungProjekt.collection;

import com.arangodb.springframework.annotation.Edge;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

@Edge("event")
@Setter
@Getter
public class Event {
    @Id
    private String id;

    private String type;

    private String _from;

    private String _to;

    @Override
    public String toString() {
        return "Event{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", _from='" + _from + '\'' +
                ", _to='" + _to + '\'' +
                '}';
    }
}
