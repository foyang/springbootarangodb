package hz.arangoDb.sldbVorlesungProjekt.collection;

import com.arangodb.springframework.annotation.Document;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

@Document("cars")
@Setter
@Getter
public class Car {
    @Id
    private String id;

    private String model;

    private String color;

    private int year;

    public Car(String model, String color, int year) {
        this.model = model;
        this.color = color;
        this.year = year;
    }

    public Car() {
    }
}
