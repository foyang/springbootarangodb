package hz.arangoDb.sldbVorlesungProjekt.collection.repository;

import com.arangodb.springframework.repository.ArangoRepository;
import hz.arangoDb.sldbVorlesungProjekt.collection.Car;

import java.util.List;

public interface CarRepository extends ArangoRepository<Car, String> {
    List<Car> findAllByColor(String color);
    List<Car> findAllByModel(String model);
    Car findCarById(String id);
}
