package hz.arangoDb.sldbVorlesungProjekt.collection.repository;

import com.arangodb.springframework.repository.ArangoRepository;
import hz.arangoDb.sldbVorlesungProjekt.collection.Event;

import java.util.List;

public interface EventRepository extends ArangoRepository<Event, String> {
    List<Event> findAllBy_from(String _from);
}
