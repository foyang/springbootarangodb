package hz.arangoDb.sldbVorlesungProjekt.collection;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class GraphModelEvent {
    private String model;
    private int count;
}
