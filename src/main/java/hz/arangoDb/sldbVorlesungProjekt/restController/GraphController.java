package hz.arangoDb.sldbVorlesungProjekt.restController;

import hz.arangoDb.sldbVorlesungProjekt.collection.payload.GraphPayload;
import hz.arangoDb.sldbVorlesungProjekt.service.CarService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("graph")
@Api( description="This Controller manages everything related to Graph and Cars Model.")
public class GraphController {

    @Autowired
    private CarService carService;

    @GetMapping("model/{model}")
    public GraphPayload getGraphByModelAndAccident(@PathVariable("model") String model){
        return carService.getGraphFromCarByModel(model.toUpperCase());
    }
}
