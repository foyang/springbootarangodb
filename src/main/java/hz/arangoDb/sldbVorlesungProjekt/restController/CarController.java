package hz.arangoDb.sldbVorlesungProjekt.restController;

import hz.arangoDb.sldbVorlesungProjekt.collection.Car;
import hz.arangoDb.sldbVorlesungProjekt.collection.payload.CarPayload;
import hz.arangoDb.sldbVorlesungProjekt.collection.payload.CarsPayload;
import hz.arangoDb.sldbVorlesungProjekt.collection.payload.Message;
import hz.arangoDb.sldbVorlesungProjekt.collection.repository.CarRepository;
import hz.arangoDb.sldbVorlesungProjekt.service.CarService;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@CrossOrigin
@Api( description="This Controller manages everything related to Cars.")
@RestController
public class CarController {
    private final Logger log = LoggerFactory.getLogger(CarController.class);

    private final CarService carService;

    @Autowired
    private CarRepository carRepository;

    public CarController(CarService carService) {
        this.carService = carService;
    }

    @PostMapping("/car")
    public CarPayload create(@RequestBody @Valid Car car) {
        CarPayload carPayload = new CarPayload();
        long time = System.currentTimeMillis();
        carPayload.setColor(car.getColor());
        carPayload.setId(car.getId());
        carPayload.setModel(car.getModel());
        carPayload.setYear(car.getYear());
        try{
            carRepository.save(car);
        } finally {
            time = System.currentTimeMillis() - time;
            long seconds = TimeUnit.MILLISECONDS.toSeconds(time);
            if (seconds != 0){
                carPayload.setTime(seconds);
            } else {
                carPayload.setTime(time);
            }
        }
        return carPayload;
    }

    @GetMapping("/cars")
    public CarsPayload list() {
        CarsPayload carsPayload = new CarsPayload();
        long time = 0;
        Iterable<Car> cars;
        try {
            time = System.currentTimeMillis();
             cars = carRepository.findAll();
        } finally {
            time = System.currentTimeMillis() - time;
                carsPayload.setTime(time);
        }
        carsPayload.setCars((List<Car>) cars);
        return carsPayload;
    }

    @DeleteMapping("/cars")
    public void deleteCars() {
        carService.deleteAllCars(carService.findAll());
    }

    @GetMapping("cars/{color}")
    public CarsPayload getCarsByColor(@PathVariable("color") String color){
        long time = 0;
        List<Car> allByColor;
        CarsPayload carsPayload = new CarsPayload();
        try {
            time = System.currentTimeMillis();
            allByColor = carRepository.findAllByColor(color);
        } finally {
            time = System.currentTimeMillis() - time;
            carsPayload.setTime(time);
        }
        carsPayload.setCars(allByColor);
        return carsPayload;
    }

    @GetMapping("/car/{id}")
    public CarPayload getCarById(@PathVariable("id") String id){
        CarPayload carPayload = new CarPayload();
        Optional<Car> carByID;
        long time = 0;
        try {
            time = System.currentTimeMillis();
            carByID = carRepository.findById("cars/"+id);
        } finally {
            time = System.currentTimeMillis() - time;
            carPayload.setTime(time);
        }
        if (carByID.isPresent()){
            Car car = carByID.get();
            carPayload.setYear(car.getYear());
            carPayload.setModel(car.getModel());
            carPayload.setId(car.getId());
            carPayload.setColor(car.getColor());
            return carPayload;
        }
        return null;
    }

    @PutMapping("car/{id}")
    public Car updateCarById(@PathVariable("id") String id, @RequestBody Car car){
        Car carById = carRepository.findCarById(id);
        carById.setColor(car.getColor());
        carById.setModel(car.getModel());
        carById.setYear(car.getYear());
        return carRepository.save(carById);
    }

    @DeleteMapping("car/{id}")
    public Message deleteCarById(@PathVariable("id") String id){
        Car car = carRepository.findCarById(id);
        Message message = new Message();
        if (car == null){
            message.setMessage("Car id: "+id+" not found");
        } else {
            message.setMessage("Car with id: "+id+" delete success");
            carRepository.deleteById(id);
        }
        return message;
    }
}
