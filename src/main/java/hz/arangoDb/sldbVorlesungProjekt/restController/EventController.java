package hz.arangoDb.sldbVorlesungProjekt.restController;

import hz.arangoDb.sldbVorlesungProjekt.collection.Car;
import hz.arangoDb.sldbVorlesungProjekt.collection.Event;
import hz.arangoDb.sldbVorlesungProjekt.collection.payload.EventPayload;
import hz.arangoDb.sldbVorlesungProjekt.collection.payload.EventsPayload;
import hz.arangoDb.sldbVorlesungProjekt.service.EventService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@CrossOrigin
@RestController
@Api( description="This Controller manages everything related to Events.")
public class EventController {

    private final Logger log = LoggerFactory.getLogger(EventController.class);

    private final EventService eventService;

    private long time = System.currentTimeMillis();

    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @PostMapping("/event")
    public EventPayload create(@RequestBody @Valid Event event) {
        EventPayload eventPayload = new EventPayload();
        try {
            eventPayload.set_from(event.get_from());
            eventPayload.set_to(event.get_to());
            eventPayload.setId(event.getId());
            eventPayload.setType(event.getType());
            eventService.save(event);
            return eventPayload;
        } finally {
            long seconds = TimeUnit.MILLISECONDS.toSeconds(time);
            if (seconds != 0){
                eventPayload.setTime(seconds);
            } else {
                eventPayload.setTime(time);
            }
        }
    }

    @ApiOperation(value = "This method makes it possible to create random events according to the number given in parrametre.")
    @PostMapping("/add/{numberOfEvent}/events")
    public List<Event> saveMultiEvent(@PathVariable("numberOfEvent") int numberOfEvent, ServletRequest req){
        List<Car> carRandomList1 = eventService.getRandomCar(numberOfEvent);
        List<Car> carRandomList2 = eventService.getRandomCar(numberOfEvent);
        List<Event> eventList = new ArrayList<>();
        EventsPayload eventsPayload = new EventsPayload();
        try {
            for (int i = 0; i<carRandomList1.size();i++){
                    Event event = new Event();
                    event.setType("ACCIDENT");
                    event.set_from("cars/"+carRandomList1.get(i).getId());
                    event.set_to("cars/"+carRandomList2.get(i).getId());
                    eventService.save(event);
                    eventList.add(event);
            }
            eventsPayload.setEvents(eventList);
        } finally {
            long seconds = TimeUnit.MILLISECONDS.toSeconds(time);
            if (seconds != 0){
                eventsPayload.setTime(seconds);
            } else {
                eventsPayload.setTime(time);
            }
        }
        return eventList;
    }

    @GetMapping("/events")
    public EventsPayload list() {
        EventsPayload eventsPayload = new EventsPayload();
        Iterable<Event> events;
        try {
            time = System.currentTimeMillis();
            events = eventService.findAll();
        } finally {
            time = System.currentTimeMillis() - time;
            eventsPayload.setTime(time);
        }
        eventsPayload.setEvents((List<Event>) events);
        return eventsPayload;
    }
    @DeleteMapping("/events")
    public void deleteEvents() {
        eventService.delete(eventService.getEvents());
    }


}
